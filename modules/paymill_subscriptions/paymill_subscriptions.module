<?php

/**
 * Implements hook_menu().
 */
function paymill_subscriptions_menu() {
  // List existing offers, subscriptions etc
  $items['admin/config/services/paymill/offers'] = array(
    'title' => 'Offers',
    'description' => 'Configure Paymill Offers.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -8,
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
  );
  $items['admin/config/services/paymill/subscriptions'] = array(
    'title' => 'Subscriptions',
    'description' => 'Configure Paymill Subscriptions.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -7,
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_views_api().
 */
function paymill_subscriptions_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'paymill_subscriptions') . '/includes/views',
  );
}

function paymill_subscriptions_field_access($op, $field, $entity_type, $entity, $account) {
  if ($entity_type == 'commerce_product') {
    if ($field['field_name'] == 'paymill_offer_id') {
      if ($op == 'edit') {
        return FALSE;
      }
    }
  }
}

//function paymill_subscriptions_form_alter($form, $form_state, $form_id) {
////  dsm($form_id);
////  dsm($form);
//  if ($form_id == 'commerce_product_ui_product_form') {
//    if ($form['#bundle'] == 'paymill_offer') {
//      $form['paymill_offer_id']['#access'] = FALSE;
//    }
//  }
//}
//
///**
// * implenets hook_inline_entity_form_entity_form_alter to hide paymill offer id
// * field
// */
//function paymill_subscriptions_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {
////  dsm($entity_form);
////  dsm($form_state);
//  if ($entity_form['#bundle'] == 'paymill_offer') {
//    $entity_form['paymill_offer_id']['#access'] = FALSE;
//  }
//}

/**
 * Implements hook_node_validate
 * Checks that we can reach paymill
 */
function paymill_subscriptions_node_validate($node, $form, $form_state) {
  if ($node->type == 'paymill_offer') {
    if (!paymill_validate()) {
      drupal_set_message(t('Failed validation with Paymill. Please make sure you have filled in your !details.', array(
        '!details' => l(t('Paymill API Keys'), 'admin/config/services/paymill'),
      )), 'error');
    }
  }
}

/**
 * Implements hook_commerce_product_insert()
 * Reacts on a paymill offer being created and does the necessary with paymill
 * to create a new offer
 */
function paymill_subscriptions_commerce_product_insert($entity) {
  if ($entity->type == 'paymill_offer') {
    $params = array(
      'amount' => $entity->commerce_price['und'][0]['amount'],
      'currency' => $entity->commerce_price['und'][0]['currency_code'],
      'interval' => $entity->paymill_offer_interval['und'][0]['value'],
      'name' => $entity->paymill_offer_name['und'][0]['value'],
      'trial_period_days' => $entity->paymill_offer_trial['und'][0]['value']
    );
    $result = paymill_create_offer($params);
    if (isset($result['error'])) {
      drupal_set_message(t('Failed creating offer with Paymill. Please make sure you have filled in your !details and that all form fields are valid.', array(
        '!details' => l(t('Paymill API Keys'), 'admin/config/services/paymill'),
      )), 'error');
      return FALSE;
    }
    else {
      $entity->paymill_offer_id['und'][0]['value'] = $result['id'];
      entity_save('commerce_product', $entity);
      $interval = $result['interval'] == 'week' ? '604800' : $result['interval'] == 'month' ? '2592000' : '31536000';
      paymill_subscriptions_subs_type_create($entity->product_id, $result['name'], $result['name'],
        t('Automatically created by Paymill Subscriptions Module.'),
        $interval, $interval, 1, $result);
    }
  }
}

function paymill_subscriptions_commerce_entity_access($op, $entity, $account) {
  if (isset($entity->type) && ($entity->type == 'paymill_offer')) {
    if ($op == 'update') {
      drupal_set_message(t('You can add description texts, new fields through manage fields etc. However, the actual Paymill Offer product cannot and should not be edited, only created or deleted (delete with caution if you have users running subscriptions on this offer. By default paymill will not let you delete an offer which has active subscriptions but we will remove it from your drupal installation.)'), 'warning');
      return FALSE;
    }
  }
}

function paymill_subscriptions_commerce_product_delete($entity) {
  if ($entity->type == 'paymill_offer') {
    $result = paymill_delete_offer($entity->paymill_offer_id['und'][0]['value']);
    if (isset($result['error'])) {
      drupal_set_message(t('Failed deleting offer with Paymill. It is most likely you have some subscriptions active with this offer still. We have removed the offer from your drupal installation though.'), 'warning');
    }
  }
}

function paymill_subscriptions_subs_type_create($product_id, $name = '', $label = '', $description = '', $length = '2592000', $grace = '0', $default_status = '0', $data = array()) {
  // Create our subs type
  $sub_type = new SubsType();
  $sub_type->name = $name;
  $sub_type->label = $label;
  $sub_type->description = $description;
  $sub_type->length = $length;
  $sub_type->grace = $grace;
  $sub_type->default_status = $default_status;
  $sub_type->module = 'paymill_subscriptions';
  $sub_type->data = $data;
  subs_type_save($sub_type);

  foreach (_paymill_subscriptions_subs_installed_instances($product_id, $name) as $field_name => $instance_detail) {
    // Look for existing instance.
    $instance = field_info_instance($instance_detail['entity_type'], $field_name, $instance_detail['bundle']);

    if (empty($instance)) {
      field_create_instance($instance_detail);
    }
  }
}

function _paymill_subscriptions_subs_installed_instances($product_id, $bundle_type) {
  return array(
    'paymill_sub_cancel' => array(
      'bundle' => $bundle_type,
      'default_value' => array(
        0 => array(
          'value' => 0,
        ),
      ),
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'type' => 'list_default',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_cancel',
      'label' => t('Subscription cancel'),
      'widget' => array(
        'module' => 'options',
        'settings' => array(
          'display_label' => 0,
        ),
        'type' => 'options_onoff',
      ),
    ),
    'paymill_sub_canceled' => array(
      'bundle' => $bundle_type,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'date',
          'settings' => array(
            'format_type' => 'long',
            'fromto' => 'both',
            'multiple_from' => '',
            'multiple_number' => '',
            'multiple_to' => '',
          ),
          'type' => 'date_default',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_canceled',
      'label' => t('Subscription Canceled'),
      'settings' => array(
        'default_value' => 'now',
        'default_value2' => 'same',
        'default_value_code' => '',
        'default_value_code2' => '',
      ),
      'widget' => array(
        'module' => 'date',
        'settings' => array(
          'increment' => 15,
          'input_format' => 'Y-m-d H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
        'type' => 'date_text',
      ),
    ),
    'paymill_sub_id' => array(
      'bundle' => $bundle_type,
      'default_value' => NULL,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_id',
      'label' => t('Subscription id'),
      'widget' => array(
        'module' => 'text',
        'type' => 'text_textfield',
      ),
    ),
    'paymill_sub_next_capture' => array(
      'bundle' => $bundle_type,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'date',
          'settings' => array(
            'format_type' => 'long',
            'fromto' => 'both',
            'multiple_from' => '',
            'multiple_number' => '',
            'multiple_to' => '',
          ),
          'type' => 'date_default',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_next_capture',
      'label' => t('Subscription Next Capture'),
      'settings' => array(
        'default_value' => 'now',
        'default_value2' => 'same',
        'default_value_code' => '',
        'default_value_code2' => '',
      ),
      'widget' => array(
        'module' => 'date',
        'settings' => array(
          'increment' => 15,
          'input_format' => 'Y-m-d H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
        'type' => 'date_text',
      ),
    ),
    'paymill_sub_offer' => array(
      'bundle' => $bundle_type,
      'default_value' => array(
        0 => array(
          'target_id' => $product_id,
        ),
      ),
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'link' => FALSE,
          ),
          'type' => 'entityreference_label',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_offer',
      'label' => t('Subscription offer'),
      'widget' => array(
        'module' => 'options',
        'type' => 'options_select',
      ),
    ),
    'paymill_sub_payment' => array(
      'bundle' => $bundle_type,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'type' => 'entityreference_label',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_payment',
      'label' => t('Subscription payment'),
      'widget' => array(
        'module' => 'options',
        'type' => 'options_select',
      ),
    ),
    'paymill_sub_trial' => array(
      'bundle' => $bundle_type,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'date',
          'settings' => array(
            'format_type' => 'long',
            'fromto' => 'both',
            'multiple_from' => '',
            'multiple_number' => '',
            'multiple_to' => '',
          ),
          'type' => 'date_default',
        ),
      ),
      'entity_type' => 'subs',
      'field_name' => 'paymill_sub_trial',
      'label' => t('Subscription Trial'),
      'settings' => array(
        'default_value' => 'now',
        'default_value2' => 'same',
        'default_value_code' => '',
        'default_value_code2' => '',
      ),
      'widget' => array(
        'module' => 'date',
        'settings' => array(
          'increment' => 15,
          'input_format' => 'Y-m-d H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
        'type' => 'date_text',
      ),
    ),
  );
}
