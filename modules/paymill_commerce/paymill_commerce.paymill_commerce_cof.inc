<?php

/**
 * @file
 * Include payment method settings callback, redirect form callbacks...
 */

/**
 * Payment method callback: submit form.
 */
function paymill_commerce_cof_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  dsm($order);
  dsm($pane_values);
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $form = paymill_commerce_credit_card_form($order);
  // Load Paymill bridge.
  $key = paymill_get_public_key();
  $settings = array('publicKey' => $key);
  $form['commerce_payment']['payment_details']['credit_card']['#attached']['js'][] = PAYMILL_BRIDGE;
  $form['commerce_payment']['payment_details']['credit_card']['#attached']['js'][] = array(
    'data' => array('commercePaymill' => $settings),
    'type' => 'setting'
  );
  $form['commerce_payment']['payment_details']['credit_card']['#attached']['js'][] = drupal_get_path('module', 'paymill_commerce') . '/paymill_commerce.js';
  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function paymill_commerce_cof_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  // Check whether its a new card or card on file
  if (!isset($pane_values['cardonfile']) || isset($pane_values['cardonfile']) && $pane_values['cardonfile'] == 'new') {
    if (empty($pane_values['credit_card']['token'])) {
      drupal_set_message(t('No Paymill token received. Your card has not been charged. Please enable JavaScript to complete your payment.'), 'error');
      return FALSE;
    }
  }
}

/**
 * Payment method callback: submit form submission.
 */
function paymill_commerce_cof_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  // If cof was submitted, skip setting up a payment and just use the remote_id
  if (isset($pane_values['cardonfile']) && $pane_values['cardonfile'] != 'new') {
    $card_data = commerce_cardonfile_data_load($pane_values['cardonfile']);
    paymill_commerce_transaction($payment_method, $order, $charge, $card_data['remote_id']);
    return TRUE;
  }
  $order->data['paymill_commerce'] = $pane_values;

  // Get client ID
  $client_id = paymill_commerce_get_client($order);
  if (!$client_id) {
    return FALSE;
  }

  // Create payment card
  $payment_params = array(
    'token' => $pane_values['credit_card']['token'],
    'client' => $client_id,
  );
  $payment = paymill_create_payment($payment_params);
  if (isset($payment['error'])) {
    drupal_set_message(check_plain($payment['error']), 'error');
    return FALSE;
  }

  $save_card = FALSE;
  if ($pane_values['credit_card']['cardonfile_store'] == 1) {
    $save_card = TRUE;
  }

  paymill_commerce_transaction($payment_method, $order, $charge, $payment['id'], $save_card);
}

function paymill_commerce_cof_save_new_card($order, $payment_method, $transaction) {
  // Card on file parameters.
  $new_card_data = array();
  // uid: the user ID of the account the card data is being stored for.
  $new_card_data['uid'] = $order->uid;
  // payment_method: the name of the payment method the card was used for.
  $new_card_data['payment_method'] = $payment_method['method_id'];
  // instance_id: the payment method instance ID containing the credentials
  // that will be used to reuse the card on file.
  $new_card_data['instance_id'] = $payment_method['instance_id'];
  $ta = paymill_get_transaction($transaction->remote_id);
  // remote_id: the remote ID to the full card data at the payment gateway.
  $new_card_data['remote_id'] = $ta['payment']['id'];
  $new_card_data['card_type'] = $ta['payment']['card_type'];
  // card_name: the name of the cardholder.
  $new_card_data['card_name'] = $ta['payment']['card_holder'];
  // card_number: the last 4 digits of the credit card number.
  $new_card_data['card_number'] = $ta['payment']['last4'];
  // card_exp_month: the numeric representation of the expiration month.
  $new_card_data['card_exp_month'] = $ta['payment']['expire_month'];
  // card_exp_year: the four digit expiration year.
  $new_card_data['card_exp_year'] = $ta['payment']['expire_year'];
  // status: integer status of the card data: inactive (0), active (1), or
  // active and not deletable (2).
  $new_card_data['status'] = 1;
  // Save and log the creation of the new card on file.
  $save = commerce_cardonfile_data_save($new_card_data);
  watchdog('paymill_commerce', 'COF, with remote ID @profile_id, added for user @uid.', array(
    '@profile_id' => $new_card_data['remote_id'],
    '@uid' => $order->uid
  ));
}

/**
 * Loads stored card data by user and ID.
 *
 * @param int $uid
 *   The user ID of the user's stored card data to load.
 * @param int $card_id
 *   The local ID of the stored card data to load.
 *
 * @return array
 *   An array containing the specified card data or FALSE if the specified card
 *   data does not exist.
 */
function paymill_commerce_load_cardonfile_data($uid, $card_id) {
  return db_select('commerce_card_data', 'ccd')
    ->fields('ccd')
    ->condition('ccd.uid', $uid)
    ->condition('ccd.card_id', $card_id)
    ->execute()
    ->fetchAssoc();
}