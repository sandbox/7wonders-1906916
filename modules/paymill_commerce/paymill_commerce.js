/**
 * @file
 * Generates Paymill tokens and handles client side errors.
 */

(function ($) {

    Drupal.behaviors.commercePaymill = {
        attach:function (context, settings) {
            window.PAYMILL_PUBLIC_KEY = settings.commercePaymill.publicKey;
            $('#commerce-checkout-form-review').submit('click', paymillSubmit);
        }
    };

    function paymillSubmit(event) {

        var submitButton = $('.form-submit');
        var processing = $('.checkout-processing');
        var selected = $('input[name="commerce_payment[payment_method]"]:checked').val();
        var cardHolderName = $('input[name="commerce_payment[payment_details][credit_card][owner]"]').val();
        var cardNumber = $('input[name="commerce_payment[payment_details][credit_card][number]"]').val();
        var expMonth = $('select[name="commerce_payment[payment_details][credit_card][exp_month]"]').val();
        var expYear = $('select[name="commerce_payment[payment_details][credit_card][exp_year]"]').val();
        var securityCode = $('input[name="commerce_payment[payment_details][credit_card][code]"]').val();
        var orderAmount = $('input[name="commerce_payment[payment_details][credit_card][amount]"]').val();
        var orderCurrency = $('input[name="commerce_payment[payment_details][credit_card][currency]"]').val();
        var cofChecked = $('input[name="commerce_payment[payment_details][cardonfile]"]:checked');

        submitButton.attr("disabled", "disabled");

        if (selected != ('paymill_commerce_cof|commerce_payment_paymill_commerce_cof' || 'paymill_commerce|commerce_payment_paymill_commerce')) return;

        if (cofChecked.length > 0 && cofChecked.val() != 'new') return;

        if (false == paymill.validateCardNumber(cardNumber)) {
            formSetError(paymillGetError('field_invalid_card_number'));
            submitButton.removeAttr("disabled");
            processing.hide();
            event.preventDefault();
            return false;
        }

        if (false == paymill.validateExpiry(expMonth, expYear)) {
            formSetError(paymillGetError('field_invalid_card_exp'));
            submitButton.removeAttr("disabled");
            processing.hide();
            event.preventDefault();
            return false;
        }

        if (false == paymill.validateCvc(securityCode)) {
            formSetError(paymillGetError('field_invalid_card_cvc'));
            submitButton.removeAttr("disabled");
            processing.hide();
            event.preventDefault();
            return false;
        }

        event.preventDefault();
        paymillGetToken(cardHolderName, cardNumber, expMonth, expYear, securityCode, orderAmount, orderCurrency);
        return false;
    }

    function paymillGetToken(cardHolderName, cardNumber, expMonth, expYear, securityCode, orderAmount, orderCurrency) {
        paymill.config('3ds_cancel_label', Drupal.t('Cancel'));
        paymill.createToken({
                number:cardNumber,
                exp_month:expMonth,
                exp_year:expYear,
                cvc:securityCode,
                cardholder:cardHolderName,
                amount_int:orderAmount,
                currency:orderCurrency
            },
            paymillResponseHandler);
    }

    function paymillResponseHandler(error, result) {
        if (error) {
            if (error.apierror == '3ds_cancelled') {
                return;
            }
            var errorText = paymillGetError(error.apierror);
            formSetError(errorText);
            $('.form-submit').removeAttr("disabled");
            $('.checkout-processing').hide();
        } else {
            $('input[name="commerce_payment[payment_details][credit_card][token]"]').val(result.token);
            $('#commerce-checkout-form-review').get(0).submit();
        }
    }

    function formSetError(errorText) {
        var comPayError = $('#edit-commerce-payment-error');
        if (!comPayError.length) {
            $('#edit-commerce-payment > .fieldset-wrapper').prepend('<div id="edit-commerce-payment-error" class="messages error"></div><br />');
        }
        comPayError.html(errorText);
    }

    function paymillGetError(error) {
        switch (error) {
            case 'internal_server_error':
                return Drupal.t('Communication with Paymill failed');
                break;

            case 'invalid_public_key':
                return Drupal.t('Invalid public key');
                break;

            case 'unknown_error':
                return Drupal.t('Unknown error');
                break;

            case '3ds_cancelled':
                return Drupal.t('User cancelled 3D security password entry');
                break;

            case 'field_invalid_card_number':
                return Drupal.t('Missing or invalid credit card number');
                break;

            case 'field_invalid_card_exp_year':
                return Drupal.t('Missing or invalid expiry year');
                break;

            case 'field_invalid_card_exp_month':
                return Drupal.t('Missing or invalid expiry month');
                break;

            case 'field_invalid_card_exp':
                return Drupal.t('Please check your card expiration');
                break;

            case 'field_invalid_card_cvc':
                return Drupal.t('Missing or invalid cvc/security number');
                break;

            case 'field_invalid_card_holder':
                return Drupal.t('Missing or invalid cardholder name');
                break;

            case 'field_invalid_account_number':
                return Drupal.t('Missing or invalid bank account number');
                break;

            case 'field_invalid_account_holder':
                return Drupal.t('Missing or invalid bank account holder');
                break;

            case 'field_invalid_bank_code':
                return Drupal.t('Missing or invalid zip code');
                break;

            default:
                return Drupal.checkPlain(error.replace('_', ' '));
        }
    }

})(jQuery);
