<?php

/**
 * @file
 * Include payment method settings callback, redirect form callbacks...
 */

/**
 * Payment method callback: submit form.
 */
function paymill_commerce_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $form = paymill_commerce_credit_card_form($order);
  // Load Paymill bridge.
  $key = paymill_get_public_key();
  $settings = array('publicKey' => $key);
  $form['commerce_payment']['payment_details']['credit_card']['#attached']['js'][] = PAYMILL_BRIDGE;
  $form['commerce_payment']['payment_details']['credit_card']['#attached']['js'][] = array(
    'data' => array('commercePaymill' => $settings),
    'type' => 'setting'
  );
  $form['commerce_payment']['payment_details']['credit_card']['#attached']['js'][] = drupal_get_path('module', 'paymill_commerce') . '/paymill_commerce.js';
  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function paymill_commerce_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  if (empty($pane_values['credit_card']['token'])) {
    drupal_set_message(t('No Paymill token received. Your card has not been charged. Please enable JavaScript to complete your payment.'), 'error');
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Payment method callback: submit form submission.
 */
function paymill_commerce_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['paymill_commerce'] = $pane_values;

  // Get client ID
  $client_id = paymill_commerce_get_client($order);
  if (!$client_id) {
    return FALSE;
  }

  // Create payment card
  $payment_params = array(
    'token' => $pane_values['credit_card']['token'],
    'client' => $client_id,
  );
  $payment = paymill_create_payment($payment_params);
  if (isset($payment['error'])) {
    drupal_set_message(check_plain($payment['error']), 'error');
    return FALSE;
  }
  paymill_commerce_transaction($payment_method, $order, $charge, $payment['id']);
}
