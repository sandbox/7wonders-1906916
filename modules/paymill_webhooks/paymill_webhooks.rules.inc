<?php
/**
 * @file
 * Rules integration for the Paymill Webhooks module.
 */
/**
 * Implements hook_rules_event_info()
 */
function paymill_webhooks_rules_event_info() {
  $items = array(
    'paymill_new_webhook_event' => array(
      'label' => t('New Paymill webhook event'),
      'group' => t('Paymill'),
      'variables' => array(
        'paymill_webhook_event' => array(
          'type' => 'paymill_webhook_event',
          'label' => t('Paymill webhook event'),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_rules_data_info().
 * @see rules_core_modules()
 */
function paymill_webhooks_rules_data_info() {
  return array(
    'paymill_webhook_event' => array(
      'label' => t('Paymill webhook event'),
      'wrap' => TRUE,
      'property info' => _paymill_webhooks_event_info(),
    ),
  );
}

/**
 * Defines property info for paymill webhook events.
 */
function _paymill_webhooks_event_info() {
  return array(
    'event_type' => array(
      'type' => 'text',
      'label' => t('Paymill webhook event type'),
    ),
    'created_at' => array(
      'type' => 'integer',
      'label' => t('Webhook event created at'),
    ),
    'event_resource' => array(
      'type' => 'struct',
      'label' => t('The event resource'),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function paymill_webhooks_rules_condition_info() {
  $conditions['paymill_webhooks_compare_type'] = array(
    'label' => t('Event type comparison'),
    'parameter' => array(
      'event_type' => array(
        'type' => 'text',
        'label' => t('Paymill webhook event type'),
      ),
      'webhook_event_type' => array(
        'type' => 'text',
        'label' => t('Paymill webhook event types'),
        'description' => t('The event type that should be compared.'),
        'options list' => 'paymill_webhooks_event_types',
        'restriction' => 'input',
      ),
    ),
    'group' => t('Paymill'),
    'callbacks' => array(
      'execute' => 'paymill_webhooks_rules_compare_type',
    ),
  );

  return $conditions;
}

/**
 * Options list callback defining event types.
 */
function paymill_webhooks_event_types() {
  return array(
    'chargeback.executed' => t('chargeback.executed'),
    'refund.created' => t('refund.created'),
    'refund.succeeded' => t('refund.succeeded'),
    'refund.failed' => t('refund.failed'),
    'subscription.succeeded' => t('subscription.succeeded'),
    'subscription.failed' => t('subscription.failed'),
    'transaction.created' => t('transaction.created'),
    'transaction.succeeded' => t('transaction.succeeded'),
    'transaction.failed' => t('transaction.failed'),
  );
}

/**
 * Condition callback: compares event types.
 */
function paymill_webhooks_rules_compare_type($type, $event_type) {
  if ($type == $event_type) {
    return TRUE;
  }
  return FALSE;
}
