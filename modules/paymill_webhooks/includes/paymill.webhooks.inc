<?php

/**
 * @file
 * Paymill settings forms and administration page callbacks.
 */

/**
 * Returns the Paymill webhooks form.
 */
function paymill_webhooks_form($form, &$form_state) {
  // Checks if there is already webhooks set up for the site
  if (variable_get('paymill_webhooks_id')) {

    $form['webhooks'] = array(
      '#type' => 'fieldset',
      '#title' => t('Paymill webhooks settings'),
      '#description' => t('You already have a webhook defined.') . '<br /><br />' . json_encode(paymill_get_webhook(variable_get('paymill_webhooks_id')), JSON_PRETTY_PRINT),
      '#collapsible' => FALSE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete?')
    );
    $form['#submit'][] = 'paymill_webhooks_form_submit_delete';
  }
  else {

    $form['webhooks'] = array(
      '#type' => 'fieldset',
      '#title' => t('Paymill webhooks settings'),
      '#description' => t('Create a paymill webhook for this site.'),
      '#collapsible' => FALSE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create')
    );
    $form['#submit'][] = 'paymill_webhooks_form_submit_create';
  }

  return $form;
}

/**
 * Submit handler for paymill_settings_form().
 */
function paymill_webhooks_form_submit_delete($form, &$form_state) {
  paymill_webhooks_delete(variable_get('paymill_webhooks_id'));
}

/**
 * Submit handler for paymill_settings_form().
 */
function paymill_webhooks_form_submit_create($form, &$form_state) {
  paymill_webhooks_create(array_values(paymill_webhooks_choice()));
}
