<?php

/**
 * @file
 * Paymill settings forms and administration page callbacks.
 */

/**
 * Returns the site-wide Paymill settings form.
 */
function paymill_settings_form($form, &$form_state) {
  // Add form elements to collect default account information.
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default account settings'),
    '#description' => t('Configure this information based on the "API Keys" section within the Paymill account settings.'),
    '#collapsible' => TRUE,
  );

  $form['account']['test_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private test key'),
    '#size' => 40,
    '#maxlength' => 32,
    '#default_value' => variable_get('paymill_test_private_key', ''),
  );

  $form['account']['test_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public test key'),
    '#size' => 40,
    '#maxlength' => 32,
    '#default_value' => variable_get('paymill_test_public_key', ''),
  );

  $form['account']['live_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private live key'),
    '#size' => 40,
    '#maxlength' => 32,
    '#default_value' => variable_get('paymill_live_private_key', ''),
  );

  $form['account']['live_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public live key'),
    '#size' => 40,
    '#maxlength' => 32,
    '#default_value' => variable_get('paymill_live_public_key', ''),
  );

  $form['account']['mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction mode'),
    '#options' => array(
      'test' => t('Test'),
      'live' => t('Live'),
    ),
    '#default_value' => variable_get('paymill_mode', 'test'),
  );

  $form['account']['capture_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Capture mode'),
    '#options' => array(
      'capture' => t('Capture'),
      'auth' => t('Pre-authorize only'),
    ),
    '#default_value' => variable_get('paymill_capture_mode', 'capture'),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'paymill_settings_form_submit';
  return $form;
}

/**
 * Submit handler for paymill_settings_form().
 */
function paymill_settings_form_submit($form, &$form_state) {
  variable_set('paymill_test_private_key', $form_state['values']['test_private_key']);
  variable_set('paymill_test_public_key', $form_state['values']['test_public_key']);
  variable_set('paymill_live_private_key', $form_state['values']['live_private_key']);
  variable_set('paymill_live_public_key', $form_state['values']['live_public_key']);
  variable_set('paymill_mode', $form_state['values']['mode']);
  variable_set('paymill_capture_mode', $form_state['values']['capture_mode']);
  if (!paymill_validate()) {
    drupal_set_message(t('Failed validation with Paymill. Please check your API Keys'));
  }
}
